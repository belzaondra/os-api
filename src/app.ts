
import process from "process"
import os from "os"
import dns from "dns"
import express from "express"
import {getDiskInfoSync} from "node-disk-info"


const getArch = () => {
    const arch = os.arch()
    return `Your arch is ${arch}`
}

const getCpus = () => {
    const cpus = os.cpus();
    const numOfCores = cpus.length;
    return `Your cpu is ${cpus[0].model} \nWith ${numOfCores} cores \nat ${cpus[0].speed / 1000} GHz`
}

const getRam = () => {
    const totalMem = os.totalmem();
    const usedMem = os.freemem();
    return `Your computer has total of ${(totalMem / 1024  / 1024 / 1024).toFixed(2)} GB usable ram \nYour computer is using ${(usedMem / 1024  / 1024 / 1024).toFixed(2)} GB ram \nYour computer has ${((totalMem - usedMem) / 1024  / 1024 / 1024).toFixed(2)} GB free ram`;

}

const getDiskSpace = () => {
    const disks = getDiskInfoSync()
    let info = ""
    disks.forEach(d => {
        info += `Disk name: ${d.mounted} \nUsed space: ${(d.used / 1024 / 1024 / 1024).toFixed(2)} GB \nAvailable capacity ${(d.available / 1024 / 1024 / 1024).toFixed(2)} GB`
    })
    return info
}

const getHostname = () => {
    const hostname = os.hostname();
    return `Your hostname is ${hostname}`

}

const getIP = () => {
    return new Promise((resolve, reject) => {
        dns.lookup(os.hostname(), (_err, add ,_) => {
            resolve(`Your ip is ${add}`) 
        });
    })
    
    
}

const main = () => {
    const app = express();

    app.get("/arch", (req, res) => {
        res.send(getArch())
    })

    app.get("/cpu", (req, res) => {
        res.send(getCpus())
    })

    app.get("/ram", (req, res) => {
        res.send(getRam())
    })

    app.get("/disk", (req, res) => {
        res.send(getDiskSpace())
    })

    app.get("/hostname", (req, res) => {
        res.send(getHostname())
    })

    app.get("/ip",async (req, res) => {
        res.send(await getIP())
    })


    app.listen(4000, () => {
        console.log('Server is listening on port 4000')
    })
}

main()